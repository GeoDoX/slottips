#SlotTips

Library mod for easy creation of tool-tips on inventory slots to better explain what an inventory slot is expecting.

##Download
* [Downloads](https://bitbucket.org/GeoDoX/slottips/downloads/)
* [CurseForge](http://minecraft.curseforge.com/)

##Improvements
Submit a PR, if your code improves existing features, or adds additional features, I may accept it. Please make it neat, documenting it helps!

##Mods Using SlotTips
You can submit a request to add your mod here by contacting me via #minecraftforge IRC

* None (Yet)

##Getting Started
Nothing here yet...